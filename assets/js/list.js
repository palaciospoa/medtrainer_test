const deleteLinks = document.querySelectorAll('.delete');
const message="Alert: Delete Row(s)\n Are You Sure?";

deleteLinks.forEach((deleteLink) => {
    deleteLink.addEventListener("click", function(event){
        if (!confirm(message)) {
            event.preventDefault();
        }
    });
});