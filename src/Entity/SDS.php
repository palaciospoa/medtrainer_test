<?php

declare(strict_types = 1);

namespace App\Entity;

final class SDS
{
    private $id;
    private $title;
    private $manufacturer;
    private $itemNumber;
    private $file;

    public function getId()
    {
        return $this->id;
    }

    public function title()
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function manufacturer()
    {
        return $this->manufacturer;
    }

    public function setManufacturer($manufacturer): void
    {
        $this->manufacturer = $manufacturer;
    }

    public function itemNumber()
    {
        return $this->itemNumber;
    }

    public function setItemNumber($itemNumber): void
    {
        $this->itemNumber = $itemNumber;
    }

    public function file()
    {
        return $this->file;
    }

    public function setFile($file): void
    {
        $this->file = $file;
    }

}