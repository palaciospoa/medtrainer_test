<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\SDS;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

final class SDSRepository
{
    private $repository;

    public function __construct(PaginatorInterface $paginator, EntityManagerInterface $entityManager)
    {
        $this->repository    = $entityManager->getRepository(SDS::class);
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    public function find($id): SDS
    {
        return $this->repository->find($id);
    }

    public function findAll(int $page)
    {
        $elementsPerPage=10;
        $repository = $this->repository->findAll();
        $repositoryPaginated = $this->paginator->paginate($repository, $page, $elementsPerPage);
        return  $repositoryPaginated;
    }

    public function update(SDS $sds): void
    {
        $this->entityManager->merge($sds);
        $this->entityManager->flush();
    }

    public function remove(SDS $sds): void
    {
        $this->entityManager->remove($sds);
        $this->entityManager->flush();
    }

    public function save(SDS $sds): void
    {
        $this->entityManager->persist($sds);
        $this->entityManager->flush();
    }
}