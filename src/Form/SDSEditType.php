<?php

declare(strict_types = 1);

namespace App\Form;

use App\Entity\SDS;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class SDSEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class,['label'=>'form.sds.title'])
            ->add('manufacturer',TextType::class,['label'=>'form.sds.manufacturer'])
            ->add('itemNumber',TextType::class,['label'=>'form.sds.item_number'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SDS::class,
            ]
        );
    }
}