<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\SDS;
use App\Form\SDSEditType;
use App\Form\SDSType;
use App\Repository\SDSRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class SDSController extends AbstractController
{
    private $SDSRepository;

    public function __construct(SDSRepository $SDSRepository)
    {
        $this->SDSRepository = $SDSRepository;
    }

    public function index(int $page): Response
    {
        $repositoryPaginated = $this->SDSRepository->findAll($page);
        return $this->render('list.html.twig',['SDSRepository'=>$repositoryPaginated]);
    }

    public function formSDS(Request $request): Response
    {
        $sds = new SDS();

        $form = $this->createForm(SDSType::class, $sds);
        $form->handleRequest($request);
        $uploadDir='PDFs/';
        if ($form->isSubmitted() && $form->isValid()) {
            $fileName = md5(uniqid()).".pdf";
            $file     = $form->getData()->file();
            if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
                mkdir($uploadDir, 0775, true);
            }

            if ($file->move($uploadDir, $fileName)) {
                $sds->setFile($fileName);
                $this->SDSRepository->save($sds);
                return $this->redirectToRoute('index');
            }
        }

        return $this->render(
            'form.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function editSDS(int $id, Request $request): Response
    {
        $sds = $this->SDSRepository->find($id);
        if (is_null($sds)) {
            return $this->render('notFound.html.twig');
        }
        $form = $this->createForm(SDSEditType::class, $sds);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->SDSRepository->update($sds);
            return $this->redirectToRoute('index');
        }

        return $this->render('form.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function deleteSDS(int $id)
    {
        $sds = $this->SDSRepository->find($id);

        if (is_null($sds)) {
            return $this->render('notFound.html.twig');
        }
        if(unlink('PDFs/'.$sds->file())){
            $this->SDSRepository->remove($sds);
        }

        return $this->redirectToRoute('index');
    }
}