#MEDTRAINER - EXAMEN PROGRAMADORES

¡Éxito! 


Los requerimientos del examen son:  
1. Trabajar sobre el Framework Symfony   
2. Usar Boostrap   
3. Descripción de pantallas:   
3.1 Crear el siguiente formulario debe permitir cargar un archivo
![form](doc/form.png)  

3.2 Leer de una base de datos los registros antes insertados usando paginación  
3.3 Agregar botones para descargar archivos, borrar (debe pedir confirmación) y editar registros

![form](doc/list.png)  

4  Antes de finalizar tu prueba guárdala y cuando hayas terminado envíala por email.
